import { CircularProgress } from "@mui/material";
import React from "react";

const Basic = ({ load = true, children }) => {
	return (
		<div className={"basic " + (load ? "loaded" : "loader")}>
			{load ? (
				<div className="frame">{children}</div>
			) : (
				<CircularProgress size={30} />
			)}
		</div>
	);
};

export default Basic;
