import React, { Component } from "react";
import UsersContext from "../../context/Users";
import Basic from "../../layout/Basic";
import UserCard from "../../components/user/UserCard";
import SearchForm from "../../components/user/SearchForm";

class Home extends Component {
	static contextType = UsersContext;
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		let { users } = this.context;
		return (
			<Basic>
				<SearchForm />

				<div className="userList">
					{users.map((user) => (
						<UserCard user={user} key={user.getId()}></UserCard>
					))}
				</div>
			</Basic>
		);
	}
}

export default Home;
