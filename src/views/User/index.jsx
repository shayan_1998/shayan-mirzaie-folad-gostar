import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Hireable from "../../components/user/Hireable";
import RepoItem from "../../components/user/RepoItem";
import Tags from "../../components/user/Tags";
import Basic from "../../layout/Basic";
import Api from "../../server";
import User from "../../server/models/User";

const UserPage = (props) => {
	let { userId } = useParams();
	const [pageLoad, setPageLoad] = useState(false);
	const [userInfo, setUserInfo] = useState(new User());
	const [userRepo, setUserRepo] = useState([]);

	useEffect(() => {
		fetchUser();
	}, []);
	async function fetchUser() {
		let [userInfo, userRepo] = await Promise.all([
			Api.getInstance().githubService().getUserInfo(userId),
			Api.getInstance().githubService().getUserRepos(userId),
		]);
		setUserInfo(userInfo);
		setUserRepo(userRepo);
		setPageLoad(true);
	}

	return (
		<Basic load={pageLoad}>
			<div className="row userHeadSection">
				<Link to="/" className="back">
					Back To Search
				</Link>
				<Hireable hireable={userInfo.getHireable()}></Hireable>
			</div>

			<div className="userInfo">
				<div className="col main">
					<img
						src={userInfo.getAvatarUrl()}
						alt={userInfo.getName()}
					/>
					<div>{userInfo.getName()}</div>
				</div>
				<div className="col ">
					{userInfo.getBio() && (
						<>
							<h3>Bio : </h3>
							<div>{userInfo.getBio()}</div>
						</>
					)}
					<a href={userInfo.getHtmlUrl()} className="visitButton">
						Visit Github Page
					</a>
					{userInfo.getLogin() && (
						<div>Login : {userInfo.getLogin()}</div>
					)}
					{userInfo.getCompany() && (
						<div>Company : {userInfo.getCompany()}</div>
					)}
					{userInfo.getHtmlUrl() && (
						<div>Website : {userInfo.getHtmlUrl()}</div>
					)}
				</div>
			</div>

			<Tags user={userInfo}></Tags>

			{userRepo.map((repo, index) => (
				<RepoItem
					name={repo.getName()}
					url={repo.getHtmlUrl()}
					key={index}
				></RepoItem>
			))}
		</Basic>
	);
};

export default UserPage;
