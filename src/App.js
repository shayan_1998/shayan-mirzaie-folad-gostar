import { BrowserRouter } from "react-router-dom";
import NavBar from "./components/ui/NavBar";
import Router from "./router";
import UsersContext from "./context/Users";
import { useState } from "react";

function App() {
	const [users, setUsers] = useState([]);
	function updateUsers(list) {
		setUsers(list);
	}
	const ProviderValue = {
		users,
		updateUsers,
	};

	return (
		<UsersContext.Provider value={ProviderValue}>
			<BrowserRouter>
				<NavBar />
				<Router />
			</BrowserRouter>
		</UsersContext.Provider>
	);
}

export default App;
