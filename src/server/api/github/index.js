import User from "../../models/User";
import Repo from "../../models/Repo";

export default class Github {
	http;

	constructor(http) {
		this.http = http;
	}

	async searchUsers(query) {
		return await this.http
			.get(`search/users?q=${query}`)
			.then((res) => {
				let users = [];
				res.data.items.forEach((item) => {
					let user = new User();
					user.setAvatarUrl(item.avatar_url);
					user.setLogin(item.login);
					user.setId(item.id);
					users.push(user);
				});

				return users;
			})
			.catch((err) => {
				throw err.response;
			});
	}

	async getUserInfo(username) {
		return await this.http
			.get(`users/${username}`)
			.then((res) => {
				let data = res.data;
				let user = new User();
				user.setAvatarUrl(data.avatar_url);
				user.setBio(data.bio);
				user.setBlog(data.blog);
				user.setCompany(data.company);
				user.setFollowers(data.followers);
				user.setFollowing(data.following);
				user.setHireable(data.hireable);
				user.setHtmlUrl(data.html_url);
				user.setId(data.id);
				user.setLogin(data.login);
				user.setName(data.name);
				user.setPublicGists(data.public_gists);
				user.setPublicRepos(data.public_repos);

				return user;
			})
			.catch((err) => {
				throw err.response;
			});
	}

	async getUserRepos(username) {
		return await this.http
			.get(`users/${username}/repos?per_page=5&sort=created:asc`)
			.then((res) => {
				let repos = [];
				res.data.forEach((item) => {
					let repo = new Repo(item.name, item.html_url);
					repos.push(repo);
				});

				return repos;
			})
			.catch((err) => {
				throw err.response;
			});
	}
}
