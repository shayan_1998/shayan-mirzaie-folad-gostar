export default class User {
	avatar_url = "";
	bio = "";
	blog = "";
	company = "";
	followers = null;
	following = null;
	hireable = null;
	html_url = "";
	id = 0;
	login = "";
	name = "";
	public_gists = null;
	public_repos = null;

	setAvatarUrl(avatarUrl) {
		this.avatar_url = avatarUrl;
	}
	getAvatarUrl() {
		return this.avatar_url;
	}

	setBio(bio) {
		this.bio = bio;
	}
	getBio() {
		return this.bio;
	}

	setBlog(blog) {
		this.blog = blog;
	}
	getBlog() {
		return this.blog;
	}

	setCompany(company) {
		this.company = company;
	}
	getCompany() {
		return this.company;
	}

	setFollowers(followers) {
		this.followers = followers;
	}
	getFollowers() {
		return this.followers;
	}

	setFollowing(following) {
		this.following = following;
	}
	getFollowing() {
		return this.following;
	}

	setHireable(hireable) {
		this.hireable = hireable;
	}
	getHireable() {
		return this.hireable;
	}

	setHtmlUrl(html_url) {
		this.html_url = html_url;
	}
	getHtmlUrl() {
		return this.html_url;
	}

	setId(id) {
		this.id = id;
	}
	getId() {
		return this.id;
	}

	setLogin(login) {
		this.login = login;
	}
	getLogin() {
		return this.login;
	}

	setName(name) {
		this.name = name;
	}
	getName() {
		return this.name;
	}

	setPublicGists(public_gists) {
		this.public_gists = public_gists;
	}
	getPublicGists() {
		return this.public_gists;
	}

	setPublicRepos(public_repos) {
		this.public_repos = public_repos;
	}
	getPublicRepos() {
		return this.public_repos;
	}
}
