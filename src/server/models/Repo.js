export default class Repo {
	name = "";
	html_url = "";

	constructor(name, html_url) {
		this.name = name;
		this.html_url = html_url;
	}

	getName() {
		return this.name;
	}

	getHtmlUrl() {
		return this.html_url;
	}
}
