import axios from "axios";
import Github from "./api/github";
const API_URL = process.env.REACT_APP_API_BASE_URL;
// api collections

export default class Api {
	instance = null;
	http;
	services = new Map();

	constructor() {
		this.setupAxios();
		this.setupServices();
	}

	setupAxios() {
		this.http = axios.create({
			baseURL: API_URL,
		});
		this.http.interceptors.request.use(
			async function (config) {
				return config;
			},
			function (error) {
				return Promise.reject(error);
			}
		);

		this.http.interceptors.response.use(
			function (response) {
				//2**
				// console.log(
				// 	`%c ${response.status} -> ${response.config.url}`,
				// 	"color: #77C146",
				// 	response
				// );
				return response;
			},
			async function (error) {
				//4** OR 5**
				// let { status, config } = error.response;
				// console.log(
				// 	`%c ${status} -> ${config.url}`,
				// 	"color: #d64141",
				// 	error.response
				// );

				return Promise.reject(error);
			}
		);
	}

	//add services to api services map
	setupServices() {
		this.services.set("Github", new Github(this.http));
	}

	//get an singleton instance of api class
	static getInstance() {
		if (this.instance != null) {
			return this.instance;
		} else {
			this.instance = new Api();
			return this.instance;
		}
	}

	//get access to each service
	githubService() {
		return this.services.get("Github");
	}
}
