import * as React from "react";
import { Routes, Route } from "react-router-dom";
import About from "../views/About";
import Home from "../views/Home";
import User from "../views/User";

export default function Router() {
	return (
		<div>
			<Routes>
				<Route path="/" element={<Home />}></Route>
				<Route path="/about" element={<About />}></Route>
				<Route path="/user/:userId" element={<User />}></Route>
			</Routes>
		</div>
	);
}
