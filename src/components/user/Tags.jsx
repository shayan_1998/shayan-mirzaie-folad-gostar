import React from "react";
import PropTypes from "prop-types";
import User from "../../server/models/User";
const Tags = ({ user = new User() }) => {
	return (
		<div className="tags">
			<div className="followers">Followers : {user.getFollowers()}</div>
			<div className="following">Following : {user.getFollowing()}</div>
			<div className="repos">Public Repos : {user.getPublicRepos()}</div>
			<div className="gists">Public Gists : {user.getPublicGists()}</div>
		</div>
	);
};

Tags.propsTypes = {
	user: PropTypes.instanceOf(User),
};

export default Tags;
