import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import User from "../../server/models/User";

const UserCard = ({ user = new User() }) => {
	return (
		<div className="userCard">
			<img src={user.avatar_url} alt={user.login} />
			<div className="name">{user.login}</div>
			<Link to={`user/${user.login}`} className="link">
				More
			</Link>
		</div>
	);
};
UserCard.propsTypes = {
	user: PropTypes.instanceOf(User),
};

export default UserCard;
