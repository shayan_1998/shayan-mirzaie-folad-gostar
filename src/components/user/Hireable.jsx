import React from "react";
import { Clear, Done } from "@mui/icons-material";
import PropTypes from "prop-types";

const Hireable = ({ hireable = false }) => {
	return (
		<div>
			hireable :
			{hireable ? (
				<Done sx={{ color: "green" }} className="icon"></Done>
			) : (
				<Clear sx={{ color: "red" }} className="icon"></Clear>
			)}
		</div>
	);
};

Hireable.propTypes = {
	hireable: PropTypes.bool,
};

export default Hireable;
