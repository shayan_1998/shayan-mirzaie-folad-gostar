import { Info } from "@mui/icons-material";
import React, { useContext, useState } from "react";
import UsersContext from "../../context/Users";
import Api from "../../server";
import Button from "../ui/Button";

const SearchForm = (props) => {
	const context = useContext(UsersContext);
	const [search, setSearch] = useState("");
	const [searchLoader, setSearchLoader] = useState(false);
	const [error, setError] = useState(false);

	function setSearchText(event) {
		setSearch(event.target.value);
	}

	async function getUsers(event) {
		event.preventDefault();
		if (search === "") {
			setError("Please enter something");
			setTimeout(() => {
				setError("");
			}, 2000);
			return;
		}

		setSearchLoader(true);
		await Api.getInstance()
			.githubService()
			.searchUsers(search)
			.then((res) => {
				context.updateUsers(res);
				setSearch("");
			})
			.catch((err) => {});
		setSearchLoader(false);
	}
	function clear() {
		context.updateUsers([]);
	}

	return (
		<form onSubmit={getUsers}>
			{error && (
				<div className="error">
					{" "}
					<Info></Info> {error}
				</div>
			)}

			<input
				type="text"
				placeholder="Search Users..."
				onChange={setSearchText}
				value={search}
			/>
			<Button
				classes="mt mb"
				onClick={getUsers}
				loading={searchLoader}
				type="submit"
			>
				Submit
			</Button>
			{context.users.length !== 0 && (
				<Button light classes="mb" onClick={clear}>
					Clear
				</Button>
			)}
		</form>
	);
};

export default SearchForm;
