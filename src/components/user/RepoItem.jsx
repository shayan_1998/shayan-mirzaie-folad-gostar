import React from "react";
import PropTypes from "prop-types";

const RepoItem = ({ name, url }) => {
	return (
		<div className="userRepo" key={name}>
			<a href={url}>{name}</a>
		</div>
	);
};

RepoItem.propTypes = {
	name: PropTypes.string,
	url: PropTypes.string,
};

export default RepoItem;
