import { CircularProgress } from "@mui/material";
import React from "react";

const Button = ({
	onClick = () => {},
	classes = "",
	loading = false,
	light = null,
	type = "button",
	children,
}) => {
	return (
		<button
			type={type}
			className={`button ` + (light ? " light " : " ") + classes}
			onClick={onClick}
		>
			{loading ? (
				<CircularProgress size={15} sx={{ color: "#fff" }} />
			) : (
				children
			)}
		</button>
	);
};

export default Button;
