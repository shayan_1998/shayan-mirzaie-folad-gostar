import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import { Link } from "react-router-dom";
import { GitHub } from "@mui/icons-material";

export default function NavBar() {
	return (
		<Box>
			<AppBar position="static" elevation={0}>
				<Toolbar sx={{ backgroundColor: "#E04A58" }}>
					<GitHub />
					<Typography
						variant="h5"
						component="div"
						sx={{ flexGrow: 1, fontWeight: "bold", paddingLeft: 1 }}
					>
						Github Search
					</Typography>
					<Link to="/" className="navLink">
						Home
					</Link>
					<Link to="/about" className="navLink">
						About
					</Link>
				</Toolbar>
			</AppBar>
		</Box>
	);
}
